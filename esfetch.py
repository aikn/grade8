"""
Index question-answers and search by queries.

Usage:
    esfetch.py indexall <path> [--host=<lh>|--port=<ep>|--index=<g8>]
    esfetch.py search <query> [--host=<lh>|--port=<ep>|--index=<g8>]

Options:
    --help          Run elasticsearch before use. 
                    Download and say >bin/elasticsearch
    --host=<lh>     [default: "localhost"] 
    --port=<ep>     [default: 9200] 
    --index=<g8>    [default: "grade8"] 

Examples:
    python esfetch.py indexall path_to_training_data
    python esfetch.py search "solar medicine"
"""

from __future__ import print_function

from docopt import docopt, DocoptExit

import string
import pandas as pd
from nltk.corpus import stopwords
from elasticsearch import Elasticsearch, SerializationError
from elasticsearch_dsl import DocType, String, Search
from elasticsearch_dsl.connections import connections

# TODO add support to index and search wiki pages including docopt option

# Configuration information
host = 'localhost'
port = 9200
qiname = 'grade8'
winame = 'wiki'
stops = stopwords.words('english')

es = Elasticsearch([{'host':host, 'port':port}])
es_engine = connections.create_connection(hosts=host,
                                          ports=port)
# Index mappings for Question and Wiki into Elasticsearch
class Question(DocType):
    question = String(analyzer='snowball',
                      fields={'raw': String(index='not_analyzed')})
    rightanswer = String(analyzer='snowball',
                      fields={'raw': String(index='not_analyzed')})
    wronganswer = String(analyzer='snowball',
                      fields={'raw': String(index='not_analyzed')})
    class Meta:
        index = qiname
        using = es_engine
Question.init()
class Wikipage(DocType):
    content = String(analyzer='snowball',
                     fields={'raw': String(index='not_analyzed')})
    
    class Meta:
        index = winame
        using = es_engine
Wikipage.init()

# Data cleaning (remove unicode characters)
def clean(instr):
    if instr is not None:
        instr = instr.decode('utf-8', 'ignore').encode('ascii', 'ignore')
        instr = string.translate(instr, None, string.punctuation)
        # instr = " ".join([word for word in instr.split() if word not in stops])
    return instr

# Processing question from AllenAI format
def pick_right_answer(fact):
    allans = set(fact.keys()[3:])
    right = 'answer'+fact['correctAnswer']
    allans = allans - set(right)

    rightans = clean(fact[right])
    alterans = clean(" ".join([fact[ans] for ans in allans]))
    return (rightans, alterans)

# Search Question index in ES
def search(query):
    q = Search(using=es_engine, index=qiname).\
        query("multi_match", 
              query=query,
              fields=["question", "rightanswer"])[:10]

    pretty_qa = lambda _hit: 'Q. ' + _hit.to_dict()['question'] + "\n" +\
        "A. " + _hit.to_dict()['rightanswer'] + "\n"
    results = '\n'.join(map( str, [pretty_qa(hit) for hit in q.execute()] ))

    return results

# Index all questions into ES from file at given path
def index_all(path):
    #path = './training_set.tsv'
    df = pd.read_table(path)
    print("Indexing begins.....")
    for item in df.iterrows():
        try:
            (rightanswer, wronganswer) = pick_right_answer(item[1])
            fact = Question(id=item[1]['id'],
                        question=clean(item[1]['question']),
                        rightanswer=rightanswer,
                        wronganswer=wronganswer)
            fact.meta.id = item[1]['id']
            fact.save()
        except SerializationError:
            print ("Failed to index {0}.\n".format(fact.meta.id))
            pass
    return "done indexing.\n"

# Docopt interface
if __name__=='__main__':
    try:
        args = docopt(__doc__)
        host = args['--host']
        port = int(args['--port'])
        index = args['--index']
        #print(args)

        if args['indexall'] is True:
            print(index_all(args['<path>']))
        elif args['search'] is True:
            print(search(args['<query>']))
    except DocoptExit as e:
        print(e.message)

